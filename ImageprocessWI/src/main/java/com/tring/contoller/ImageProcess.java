package com.tring.contoller;

import static java.util.Optional.empty;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

@RestController
@RequestMapping(value = "/img")
public class ImageProcess {

	private static final String endpointUrl = "https://s3.ap-south-1.amazonaws.com";
	private static final String bucketName = "tringbucket";
	private static int IMG_WIDTH = 1024;
	private static int IMG_HEIGHT = 768;
	private static int X = 0;
	private static int Y = 0;

	public static final Pattern FILE_TYPE_PATTERN = Pattern.compile(".*\\.([^\\.]*)");

	private static final String JPG_MIME = "image/jpeg";
	private static final String PNG_MIME = "image/png";

	private static final Map<String, String> imageTypeMime = new HashMap<>();

	static {
		imageTypeMime.put("jpg", JPG_MIME);
		imageTypeMime.put("jpeg", JPG_MIME);
		imageTypeMime.put("png", PNG_MIME);
	}

	Logger log = Logger.getLogger(ImageProcess.class.getName());

	@Autowired
	HttpServletResponse httpResponse;

	@GetMapping("/get")
	public String getImage() {
		return "Hello";
	}

	@PostMapping("/uploadFile")
	public String uploadFile(@RequestPart(value = "file") MultipartFile multipartFile) {

		String message = uploadFileToBucket(multipartFile);
		return message;
	}

	private File convertMultiPartToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	private String uploadFileTos3bucket(String fileName, File file) {
		try {
			AmazonS3 s3Client = getS3Client();
			s3Client.putObject(new PutObjectRequest(bucketName, fileName, file));
		} catch (AmazonServiceException e) {
			e.printStackTrace();
			return "uploadFileTos3bucket().Uploading failed :" + e.getMessage();
		}
		return "Uploading Successfull -> ";
	}

	private AmazonS3 getS3Client() {
		BasicAWSCredentials credentials = new BasicAWSCredentials("AKIASP5PKVM23X3GCAWL",
				"+aCywb4zQIMyuo1KHuxRzZ2NAKOFJDDCnATOFM5Y");
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
		return s3Client;
	}

	@GetMapping("/getfile")
	public byte[] downloadFile(@RequestParam String fileName, @RequestParam(required = false) Integer x,
			@RequestParam(required = false) Integer y, @RequestParam int width, @RequestParam int height)
			throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();

//		httpResponse.setContentType("image/jpeg");
		httpResponse.setHeader("Content-disposition", "attachment; filename=" + fileName);
		try {
			AmazonS3 s3Client = getS3Client();
			S3Object s3Object = s3Client.getObject(new GetObjectRequest(bucketName, fileName));
			S3ObjectInputStream objectData = s3Object.getObjectContent();
			BufferedImage srcImage = ImageIO.read(objectData);
			int srcHeight = srcImage.getHeight();
			int srcWidth = srcImage.getWidth();

			log.info("Object Key:: " + s3Object.getKey());
			Optional<String> optionalImageType = getImageType(s3Object.getKey());
			if (!optionalImageType.isPresent()) {
				return null;
			}
			String imageType = optionalImageType.get();
			int type = (srcImage.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB
					: BufferedImage.TYPE_INT_ARGB;

			log.info("x:: " + x);
			log.info("y:: " + y);
			if (x != null && y != null) {
				log.info("Both x & y not equals to null");
				BufferedImage cropImage = cropImage(srcImage, x, y, width, height);
				BufferedImage resizeImageHintJpg = resizeImageWithHint(cropImage, type);
				ImageIO.write(resizeImageHintJpg, imageType, os);
			} else {
				BufferedImage resizeImageJpg = resizeImage(srcImage, width, height, type);
				ImageIO.write(resizeImageJpg, imageType, os);
			}

			String contentType = imageTypeMime.get(imageType);
			httpResponse.setContentType(contentType);
			return os.toByteArray();
		} finally {
			os.close();
		}
	}

	public static BufferedImage deepCopy(BufferedImage bi) {
		ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}

	public static BufferedImage cropImage(BufferedImage bufferedImage, int x, int y, int width, int height) {
		bufferedImage = deepCopy(bufferedImage);
		BufferedImage croppedImage = bufferedImage.getSubimage(x, y, width, height);
		return croppedImage;
	}

	private static BufferedImage resizeImageWithHint(BufferedImage img, int type) {

		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
		Graphics2D g = resizedImage.createGraphics();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, X, Y, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();
		return resizedImage;
	}

	private String uploadFileToBucket(MultipartFile multipartFile) {

		
		String response;
		try {
			AmazonS3 s3Client = getS3Client();
			log.info("Uploading object...");
			log.info("Content type:: " + multipartFile.getContentType());
			log.info("Object key to be stored:: " + multipartFile.getOriginalFilename());

			ObjectMetadata metadata = new ObjectMetadata();
			metadata.addUserMetadata("Description", "Uploading for test purpose");

			s3Client.putObject(bucketName, multipartFile.getOriginalFilename(), multipartFile.getInputStream(),
					metadata);
			response = "Successfully uploaded";

		} catch (Exception exception) {
			exception.printStackTrace();
			response = exception.getMessage();
		}
		return response;
	}

	private static Optional<String> getImageType(String key) {
		Matcher matcher = FILE_TYPE_PATTERN.matcher(key);
		if (!matcher.matches()) {
			System.out.println("Unable to infer image type for key " + key);
			return empty();
		}
		String imageType = matcher.group(1);
		if (!imageTypeMime.keySet().contains(imageType)) {
			System.out.println("Skipping non-image " + key);
			return empty();
		}

		return Optional.of(imageType);
	}

	private static BufferedImage resizeImage(BufferedImage originalImage, int w, int h, int type) {
		// BufferedImage resizedImage = new BufferedImage(originalImage.getWidth(),
		// originalImage.getHeight(), type);
		BufferedImage resizedImage = new BufferedImage(w, h, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, X, Y, w, h, null);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.dispose();

		return resizedImage;
	}

}
