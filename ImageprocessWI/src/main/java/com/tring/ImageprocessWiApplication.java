package com.tring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageprocessWiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImageprocessWiApplication.class, args);
	}

}
